import argparse
import subprocess
import os


def main():
    # create ArgumentParser
    parser = argparse.ArgumentParser(description='product sdk build scripts')

    # add all arguments
    parser.add_argument('recipe', type=str, help='need input build recipe')
    parser.add_argument('--second_recipe', type=str, help=' build second recipe')
    parser.add_argument('--image_install', type=str, help=' build image with install package')

    #parser.add_argument('--from_src', choices=['True', 'False'] ,default=False, help='build from src, defult is false, default build from artifacts')
    parser.add_argument('--from_src',type=str, help='will set the input parameters ture and pass to esdk  ')
    parser.add_argument('--operation', type=str, choices=['build_single_recipe','build_artifacts', 'build_image','clean'], default='build_artifacts', help='build specific artifacts or full image')

    args = parser.parse_args()
    if args.from_src:
        print("from_src is true")
        os.environ['BB_ENV_PASSTHROUGH_ADDITIONS'] = str(args.from_src)
        os.environ[args.from_src]='True'

    if args.operation == 'build_image':
        if args.image_install:
            path = 'layers/poky'
            filename=str(args.recipe)+".bb"
            file_path=""
            print("filename is %s"%filename)
            for root, dirs, files in os.walk(path):
                if filename in files:
                    file_path = os.path.join(root, filename)
            print("file_path is %s"%file_path)
            with open(file_path, 'a') as file:
                file.write('CORE_IMAGE_EXTRA_INSTALL += "'+ args.image_install + '"'+ '\n')
            cmd=['devtool', 'build-image', args.recipe]
        else:
            print("please input package to install image")
    elif args.operation == 'clean':
        cmd=['devtool', 'reset', args.recipe]
    else :
        modified_recipe_path=os.path.join("workspace/appends/",args.recipe + ".bbappend")
        if not os.path.exists(modified_recipe_path):
            cmd=['devtool', 'modify', args.recipe]
            print("Modify recipe")
            subprocess.call(cmd)
        if args.operation == 'build_artifacts' or  args.operation == 'build_single_recipe':
            print("build_artifacts or build_artifacts")
            cmd=['devtool', 'build', args.recipe]
    subprocess.call(cmd)
    if args.second_recipe:
        if not os.path.exists(os.path.join("workspace/appends/",args.second_recipe + ".bbappend")):
            cmd=['devtool', 'modify', args.second_recipe]
            subprocess.call(cmd)
        cmd=['devtool', 'build', args.second_recipe]
        print("build the second rescipe")
        subprocess.call(cmd)

if __name__ == '__main__':
    main()
